import { $api } from '../../services'

export const UsersModule = {

  state: {
    users: [],
    user: {}
  },

  mutations: {
    'setUsers': (state, users) => {
      state.users = users
    },
    'setUser': (state, user) => {
      state.user = user
    }
  },

  getters: {
    users: (state) => {
      return state.users
    },

    user: (state) => {
      return state.user
    }
  },

  actions: {
    'fetchUsers': ({ commit }) => {
      return $api.users.list()
        .then((response) => {
          commit('setUsers', response)
        })
    },
    'fetchUser': ({ commit }, id) => {
      return $api.users.show(id)
        .then((response) => {
          commit('setUser', response)
        })
    }
  }
}
