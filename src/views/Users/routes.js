import Users from './Users'
import UserView from './UserView'

export const usersRoutes = [
  {
    path: '/users',
    name: 'users',
    component: Users
  },
  {
    path: '/users/:id',
    name: 'user.view',
    component: UserView
  }
]
