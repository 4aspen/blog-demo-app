import Posts from './Posts'
import PostView from './PostView'

export const postsRoutes = [
  {
    path: '/posts',
    name: 'posts',
    component: Posts
  },
  {
    path: '/posts/:id',
    name: 'post.view',
    component: PostView,
    props: true
  }
]
