import { $api } from '../../services'

export const PostsModule = {

  state: {
    posts: [],
    post: {}
  },

  mutations: {
    'setPosts': (state, posts) => {
      state.posts = posts
    },
    'setPost': (state, post) => {
      state.post = post
    },
    'newPost': (state, post) => {
      state.posts.unshift(post)
    },
    'removePost': (state, id) => {
      state.posts = state.posts.filter(post => post.id !== id)
    }
  },

  getters: {
    posts: (state) => {
      return state.posts
    },

    post: (state) => {
      return state.post
    }
  },

  actions: {
    'fetchPosts': ({ commit }) => {
      return $api.posts.list()
        .then((response) => {
          commit('setPosts', response)
        })
    },
    'fetchPost': ({ commit }, id) => {
      return $api.posts.show(id)
        .then((response) => {
          commit('setPost', response)
        })
    },
    'addPost': ({ commit }, post) => {
      return $api.posts.create(post)
        .then((response) => {
          commit('newPost', response)
        })
    },
    'deletePost': ({ commit }, id) => {
      $api.posts.delete(id)

      commit('removePost', id)
    }
  }
}
