import Home from './Home'

export const homeRoutes = [
  {
    path: '/',
    name: 'home',
    component: Home
  }
]
