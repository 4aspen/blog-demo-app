import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import ApiPlugin from './services'

Vue.use(VueAxios, axios)
Vue.use(ApiPlugin)
Vue.config.productionTip = false
Vue.router = router


new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
