import Vue from 'vue'
import Router from 'vue-router'

import { homeRoutes } from './views/Home/routes'
import { postsRoutes } from './views/Posts/routes'
import { usersRoutes } from './views/Users/routes'


let routes = []

routes = routes.concat(
  homeRoutes,
  postsRoutes,
  usersRoutes
)


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
