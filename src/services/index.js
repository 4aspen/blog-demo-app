import PostService from './PostService'
import UserService from './UserService'


export const $api = {
  posts: PostService,
  users: UserService
}

const ApiPlugin = {
  install (Vue) {
    Vue.prototype.$api = $api
  }
}

export default ApiPlugin
