import Vue from 'vue'

const UserService = {
  list () {
    return Vue.axios.get('https://jsonplaceholder.typicode.com/users')
      .then((response) => response.data)
  },

  show (id) {
    return Vue.axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then((response) => response.data)
  }
}

export default UserService
