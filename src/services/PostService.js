import Vue from 'vue'

const PostService = {
  list () {
    return Vue.axios.get('https://jsonplaceholder.typicode.com/posts')
      .then((response) => response.data)
  },

  show (id) {
    return Vue.axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((response) => response.data)
  },

  create (post) {
    return Vue.axios.post('https://jsonplaceholder.typicode.com/posts/', post)
      .then((response) => response.data)
  },

  delete (id) {
    return Vue.axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
  },
}

export default PostService
