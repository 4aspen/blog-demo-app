import Vue from 'vue'
import Vuex from 'vuex'

import { PostsModule} from './views/Posts/PostsModule'
import { UsersModule } from './views/Users/UsersModule'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    PostsModule,
    UsersModule
  }
})
